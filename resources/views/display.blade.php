<!DOCTYPE html>
<html>
<head>
	<title>Twitter API</title>
</head>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="{{asset('/css/custom.css')}}">

<body>

	@foreach($users->users as $u)

	<section class="profile-section sections">
 	<div class="container">
 	<div class="row text-center">
 		<div class="profile-main">
 			<div class="col-md-6 col-md-offset-3">
 				<div class="profile-box">
 					<div class="row">
 					<div class="col-md-4 col-sm-4 col-xs-4 no-padding">
 						<div class="img">
 							<img src="{{$u->profile_image_url}}" class="img-responsive img-thumbnail">
 						</div>
 					</div>
 					<div class="col-md-4 col-sm-4 col-xs-4 no-padding">
 						<div class="name">
 							<p>{{$u->name}}</p>
 						</div>
 					</div>
 					<div class="col-md-4 col-sm-4 col-xs-4 no-padding">
 						<div class="follower">
 							<p class="num">{{$u->favourites_count}}</p>
 							<p class="title">Followers</p>
 						</div>
 					</div>
 				</div>
 				</div>
 		</div>
 		</div>
 	</div>
 </div>
 </section>
		

		@endforeach()


		<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>
</html>